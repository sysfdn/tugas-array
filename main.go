package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/google/uuid"
)

type Game struct {
	Id    string
	Name  string
	Grade float64
}

var Games []Game

func getInput(prompt string, r *bufio.Reader) (string, error) {
	fmt.Print(prompt)
	input, err := r.ReadString('\n')

	return strings.TrimSpace(input), err
}

func gameRating(grade float64) string {
	if grade > 0.0 && grade < 2.0 {
		return "Poor"
	} else if grade >= 2 && grade <= 4 {
		return "Avarage"
	} else {
		return "Good"
	}
}

func showAllGames() {
	fmt.Println("All games that recorded on Array")
	for k, v := range Games {
		fmt.Printf("No: %d\n", k)
		fmt.Printf("- Game ID: %s\n", v.Id)
		fmt.Printf("- Game Name: %s\n", v.Name)
		fmt.Printf("- Rating : %v\n", v.Grade)
		fmt.Printf("- Description : %s\n", gameRating(v.Grade))
	}
	fmt.Printf("Total: %d games recorded\n", len(Games))
}

func addGame(name string, grade float64) {
	game := Game{
		Id:    uuid.NewString(),
		Name:  name,
		Grade: grade,
	}

	Games = append(Games, game)
}

func removeGame(uuid string) {
	for index, game := range Games {
		if game.Id == uuid {
			Games = append(Games[:index], Games[index+1:]...)
		}
	}

}

func getTop3Game() {
	sort.Slice(Games, func(i, j int) bool {
		return Games[i].Grade > Games[j].Grade
	})
	if len(Games) > 2 {
		for i := 0; i < 3; i++ {
			fmt.Printf("No: %d\n", i+1)
			fmt.Printf("- Game ID: %s\n", Games[i].Id)
			fmt.Printf("- Game Name: %s\n", Games[i].Name)
			fmt.Printf("- Rating : %v\n", Games[i].Grade)
			fmt.Printf("- Description : %s\n", gameRating(Games[i].Grade))
		}
	} else {
		fmt.Println("need more game at least 3 item in katalog!")
	}

}

func getPopularGame() {
	for i, game := range Games {
		if game.Grade >= 4 {
			fmt.Printf("No: %d\n", i+1)
			fmt.Printf("- Game ID: %s\n", game.Id)
			fmt.Printf("- Game Name: %s\n", game.Name)
			fmt.Printf("- Rating : %v\n", game.Grade)
			fmt.Printf("- Description : %s\n", gameRating(game.Grade))
		}
	}
}

func searchGameByName(name string) string {
	result := "Found a game!"
	is_found := false
	for _, game := range Games {
		if game.Name == name {
			result += fmt.Sprintf("- Game ID: %s\n", game.Id)
			result += fmt.Sprintf("- Game Name: %s\n", game.Name)
			result += fmt.Sprintf("- Rating : %v\n", game.Grade)
			result += fmt.Sprintf("- Description : %s\n", gameRating(game.Grade))
			is_found = true
		}

	}
	if is_found {
		return result
	} else {
		return "Game not found"
	}

}

func showMenu() {
	fmt.Println("=== Katalog Game! ===")
	fmt.Println("1. Add Game")
	fmt.Println("2. Remove Game")
	fmt.Println("3. Show All Games")
	fmt.Println("4. Search Game")
	fmt.Println("5. Top 3 Favorite Game")
	fmt.Println("6. All Game With Rating 4 (Good Game)")
	fmt.Println("0. Exit")
	fmt.Println("Select > ")
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	var rate float64
	var choose int
	for {
		showMenu()
		fmt.Scan(&choose)
		switch choose {
		case 1:
			fmt.Println("Wanna add a game?")
			gameName, _ := getInput("Input Name: ", reader)
			fmt.Println("Game Rate (0-5): ")
			fmt.Scan(&rate)
			addGame(gameName, rate)
			fmt.Println("Success add a new game!")

		case 2:
			fmt.Println("Wanna remove a game?")
			gameID, _ := getInput("Input game id: ", reader)
			removeGame(gameID)
			fmt.Println("Success delete a game!")

		case 3:
			showAllGames()

		case 4:
			fmt.Println("Wanna search a game?")
			gameName, _ := getInput("Input game name: ", reader)
			fmt.Println(searchGameByName(gameName))
		case 5:
			fmt.Println("== Top 3 Favorite Games ==")
			getTop3Game()
		case 6:
			fmt.Println("All Popular games")
			getPopularGame()
		case 0:
			fmt.Println("Program end...")
			os.Exit(1)
		}
	}
}
